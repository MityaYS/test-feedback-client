import { User } from './../_models/user.model';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  user: User | null = null;

  redirectLinks: any = {
    client: 'feedback',
    admin: 'feedbacks-list',
  };

  resources = {
    login: '/client/login',
    logout: '/client/logout',
    checkToken: '/client/check-token',
    registerStep1: '/register-step-1',
    registerStep2: '/register-step-2',
  };

  constructor(private httpClient: HttpClient, private router: Router) {}

  register(body: User) {
    return new Promise<void>((resolve, reject) => {
      this.httpClient.post(this.resources.registerStep1, body).subscribe(
        () => {
          resolve();
        },
        (error) => {
          reject(error.error);
        }
      );
    });
  }

  checkTokenAndGetUser(role: 'client' | 'admin' | null = null) {
    return new Promise<boolean>((resolve) => {
      if (!localStorage.getItem('token')) {
        return resolve(false);
      }
      this.httpClient.get(this.resources.checkToken).subscribe(
        (data: User) => {
          this.user = data;
          if (role && this.user.role !== role) {
            resolve(false);
            if (this.user.role)
              this.router.navigate([this.redirectLinks[this.user.role]]);
          } else {
            resolve(true);
          }
        },
        () => {
          this.logout();
          resolve(false);
        }
      );
    });
  }

  login(email: string, password: string) {
    return new Promise<void>((resolve, reject) => {
      this.httpClient.post(this.resources.login, { email, password }).subscribe(
        (data: User) => {
          this.user = data;
          localStorage.setItem('token', data.token ? data.token : '');
          this.router.navigate(['/feedback']);
          resolve();
        },
        (error) => {
          reject(error.error);
        }
      );
    });
  }

  logout() {
    this.httpClient.get(this.resources.logout).subscribe(() => {});
    this.user = null;
    localStorage.removeItem('token');
    this.router.navigate(['auth']);
  }
}
