import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class CommonService {
  errorsText: any = {
    required: 'This field is required!',
    minlength: 'Text should be longer!',
    maxlength: 'Text should be shorter!',
    emailIsBusy: 'This mail is already taken!',
    invalidEmailOrPassword: 'Invalid login or password!',
    userNotIsActive: 'Confirm your email for continue!',
    notIdentic: 'Passwords do not match!',
    default: 'Error!',
  };

  constructor() {}

  getKeysFromObject(obj: Object): string[] {
    return Object.keys(obj);
  }

  getErrorMessage(error: string): string {
    return this.errorsText[error]
      ? this.errorsText[error]
      : this.errorsText['default'];
  }

  customTrim(text: string) {
    return text.replace(/\s+/g, ' ').trim();
  }
}
