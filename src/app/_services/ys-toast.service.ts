import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class YsToastService {
  private toastContainer: HTMLElement;

  constructor() {
    this.toastContainer = document.createElement('div');
    this.toastContainer.id = 'ys-toasts';
    document.querySelector('body')?.appendChild(this.toastContainer);
  }

  private getToast() {
    let toast = document.createElement('div');
    toast.className = 'ys-toast';
    toast.innerHTML = `
    <i class="toast-icon"></i>
    <div class="toast-content">
        <div class="header">
            <span class="toast-title">Title Toast</span>
            <div class="close-toast-btn">
                <i class="flaticon-cancel"></i>
            </div>
        </div>
        <span class="toast-message">
            Text message for toast
        </span>
    </div>
          `;
    return toast;
  }

  showMessage(config: ToastConfig = new ToastConfig()) {
    let typesToast = ['success', 'error', 'warn'];
    let toast = this.getToast();

    let toastTitle = toast.querySelector('.toast-title');
    if (toastTitle) toastTitle.innerHTML = config.title;

    let toastMessage = toast.querySelector('.toast-message');
    if (toastMessage) toastMessage.innerHTML = config.text;

    if (this.toastContainer) {
      this.toastContainer.style.zIndex = '100000';
      this.toastContainer.appendChild(toast);
    }

    if (typesToast.includes(config.typeToast)) {
      let icon =
        config.typeToast == 'success'
          ? 'flaticon-tick'
          : config.typeToast == 'error'
          ? 'flaticon-error'
          : 'flaticon-warning-sign';
      toast.classList.add('toast-' + config.typeToast);

      let toastIcon = toast.querySelector('.toast-icon');
      if (toastIcon) toastIcon.classList.add(icon);
    } else toast.classList.add('toast-warn');

    toast.addEventListener('mouseover', () => {
      toast.dataset.isHover = 'true';
    });

    let closeBtn = toast.querySelector('.close-toast-btn');
    if (closeBtn) closeBtn.addEventListener('click', () => closeToast(true));

    if (!config.duration) config.duration = 2000;

    setTimeout(() => closeToast(), config.duration);

    const closeToast = (hardcore = false) => {
      if (toast.dataset.isHover && !hardcore) return;
      toast.style.animationName = 'toast-close';
      setTimeout(() => {
        if (toast && toast.parentElement)
          toast.parentElement.removeChild(toast);
        if (this.toastContainer.querySelectorAll('.ys-toast').length <= 0)
          this.toastContainer.style.zIndex = '-1';
        // toast.querySelector('.toast-title').innerHTML = '';
        // toast.querySelector('.toast-message').innerHTML = '';
      }, 600);
    };
  }
}

class ToastConfig {
  title: string = '';
  text: string = '';
  typeToast: 'success' | 'error' | 'warn' = 'success';
  duration: number = 600;
}
