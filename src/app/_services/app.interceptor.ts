import { Router } from '@angular/router';
import { map, catchError } from 'rxjs/operators';
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpResponse,
  HttpErrorResponse,
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { AuthService } from '../_services/auth.service';

@Injectable()
export class AppInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService, private router: Router) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    let headers: any = {};
    let url = req.url;
    if (req.url.startsWith('/')) {
      if (!req.headers.get('Content-Type') && !(req.body instanceof FormData)) {
        headers['Content-Type'] = 'application/json';
      }
      headers['Authorization'] = localStorage.getItem('token') || '';
      url = environment.url + req.url;
    }
    return next.handle(req.clone({ setHeaders: headers, url })).pipe(
      catchError((error: HttpErrorResponse) => {
        if (error.status == 404) this.router.navigate(['/auth/page-404']);

        return throwError(error);
      })
    );
  }
}
