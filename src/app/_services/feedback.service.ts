import { Feedback } from './../_models/feedback.model';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class FeedbackService {
  resources = {
    feedback: '/feedback',
    feedback_admin: '/admin/feedback',
  };

  constructor(private httpClient: HttpClient) {}

  createFeedback(body: FormData) {
    return this.httpClient.post(this.resources.feedback, body);
  }

  resetTimeLimit() {
    return this.httpClient.put(this.resources.feedback, {});
  }

  getFeedbacks(page: number = 1) {
    let options = {
      params: new HttpParams().append('page', page),
    };
    return this.httpClient.get(this.resources.feedback_admin, options);
  }

  toggleViewFeedback(body: Feedback) {
    return this.httpClient.put(this.resources.feedback_admin, body);
  }

  downloadFileFromLink(link: string) {
    return this.httpClient.get(link);
  }
}
