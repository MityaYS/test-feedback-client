import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class UploadFilesService {
  URL = {
    file: environment.url + 'upload_file',
  };

  constructor(private http: HttpClient) {}

  fileUpload(fileInput: any) {
    let fileData = <File>fileInput.target.files[0];

    return fileData;

    // return this.postFile(fileData);
  }

  postFile(fileData: File, param = 'xlsx') {
    const formData = new FormData();
    formData.append('file', fileData);

    const options = {
      params: new HttpParams().append('param', param),
    };

    return this.http.post(this.URL.file, formData, options);
  }
}
