import { AuthAdminGuard } from './_services/auth_admin.guard';
import { AuthClientGuard } from './_services/auth_client.guard';
import { MessagePageComponent } from './_pages/message-page/message-page.component';
import { AuthPageComponent } from './_pages/auth-page/auth-page.component';
import { RegisterPageComponent } from './_pages/register-page/register-page.component';
import { FeedbackPageComponent } from './_pages/feedback-page/feedback-page.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'feedback',
    component: FeedbackPageComponent,
    canActivate: [AuthClientGuard],
  },
  {
    path: 'register',
    component: RegisterPageComponent,
  },
  {
    path: 'auth',
    component: AuthPageComponent,
  },
  {
    path: 'message/:message',
    component: MessagePageComponent,
  },
  {
    path: 'feedbacks-list',
    loadChildren: () =>
      import('./_pages/feedbacks-list/feedbacks-list.module').then(
        (m) => m.FeedbacksListModule
      ),
    canActivate: [AuthAdminGuard],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
