export class User {
  id?: number;
  name?: string = '';
  email?: string = '';
  password?: string = '';
  token?: string = '';
  role?: 'client' | 'admin' = 'client';
  last_date_request?: Date | null = null;
}
