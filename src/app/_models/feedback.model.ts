import { User } from './user.model';

export class Feedback {
  id?: number;
  subject?: string = '';
  message?: string = '';
  date_create?: Date = new Date();
  file_link?: string = '';
  fileData?: any = '';
  is_viewed?: boolean = false;
  user?: User;
}
