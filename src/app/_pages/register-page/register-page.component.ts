import { Router } from '@angular/router';
import { User } from './../../_models/user.model';
import { AuthService } from './../../_services/auth.service';
import { CommonService } from './../../_services/common.service';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { comparePasswords } from 'src/app/_utils/form_Validators';

@Component({
  selector: 'app-register-page',
  templateUrl: './register-page.component.html',
  styleUrls: ['./register-page.component.scss'],
})
export class RegisterPageComponent implements OnInit {
  registerForm = new FormGroup(
    {
      name: new FormControl('', [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(32),
      ]),
      email: new FormControl('', [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(120),
      ]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(32),
      ]),
      repeatPassword: new FormControl('', [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(32),
      ]),
    },
    { validators: comparePasswords }
  );

  errorsTrigger = false;
  serverError = '';

  constructor(
    public commonService: CommonService,
    private authService: AuthService,
    private router: Router
  ) {}

  ngOnInit(): void {}

  register(): void {
    this.errorsTrigger = true;
    console.log(this.registerForm.controls.repeatPassword.errors);
    if (this.registerForm.valid) {
      let body: User = {
        name: this.commonService.customTrim(
          this.registerForm.controls.name.value
        ),
        email: this.commonService.customTrim(
          this.registerForm.controls.email.value
        ),
        password: this.commonService.customTrim(
          this.registerForm.controls.password.value
        ),
      };

      this.registerForm.disable();

      this.authService.register(body).then(
        (data: any) => {
          this.registerForm.enable();
          this.router.navigate(['message', 'confirm-email']);
        },
        (error: any) => {
          this.serverError = error.type;
          this.registerForm.enable();
        }
      );
    }
  }
}
