import { AuthService } from 'src/app/_services/auth.service';
import { CommonService } from './../../_services/common.service';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-auth-page',
  templateUrl: './auth-page.component.html',
  styleUrls: ['./auth-page.component.scss'],
})
export class AuthPageComponent implements OnInit {
  authForm = new FormGroup({
    email: new FormControl('', [
      Validators.required,
      Validators.minLength(0),
      Validators.maxLength(120),
    ]),
    password: new FormControl('', [
      Validators.required,
      Validators.minLength(0),
      Validators.maxLength(32),
    ]),
  });

  errorsTrigger = false;
  serverError = '';

  constructor(
    public commonService: CommonService,
    private authService: AuthService
  ) {}

  ngOnInit(): void {}

  auth() {
    this.errorsTrigger = true;
    if (this.authForm.valid) {
      this.authForm.disable();

      this.authService
        .login(
          this.authForm.controls.email.value,
          this.authForm.controls.password.value
        )
        .then(
          (data: any) => {
            this.authForm.enable();
          },
          (error) => {
            this.serverError = error.type;
            this.authForm.enable();
          }
        );
    }
  }

  resetErrors() {
    this.errorsTrigger = false;
    this.serverError = '';
  }
}
