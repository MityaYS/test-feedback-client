import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-message-page',
  templateUrl: './message-page.component.html',
  styleUrls: ['./message-page.component.scss'],
})
export class MessagePageComponent implements OnInit {
  messages: any = {
    'confirm-email': {
      title: 'Confirm your email!',
      message:
        'An account confirmation message has been sent to your email, follow the link to continue registration!',
    },
    'success-auth': {
      title: 'You have completed registration!',
      message:
        'You have successfully completed registration, to continue, click the button below!',
      actionLabel: 'Go to login',
      action: () => {
        this.router.navigate(['/auth']);
      },
    },
  };

  activeMessage: any = null;

  constructor(private route: ActivatedRoute, private router: Router) {}

  ngOnInit(): void {
    let message = this.route.snapshot.params['message'];
    if (message && Object.keys(this.messages).includes(message)) {
      this.activeMessage = this.messages[message];
    }
  }
}
