import { AuthService } from 'src/app/_services/auth.service';
import { UploadFilesService } from './../../_services/uploadFiles.service';
import { YsToastService } from './../../_services/ys-toast.service';
import { FeedbackService } from './../../_services/feedback.service';
import { Feedback } from './../../_models/feedback.model';
import { CommonService } from './../../_services/common.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-feedback-page',
  templateUrl: './feedback-page.component.html',
  styleUrls: ['./feedback-page.component.scss'],
})
export class FeedbackPageComponent implements OnInit {
  feedbackForm = new FormGroup({
    subject: new FormControl('', [
      Validators.required,
      Validators.minLength(6),
      Validators.maxLength(120),
    ]),
    message: new FormControl('', [
      Validators.required,
      Validators.minLength(10),
      Validators.maxLength(500),
    ]),
    fileData: new FormControl(null),
  });

  errorsTrigger = false;
  remainingTimeError = 0;

  constructor(
    public commonService: CommonService,
    public authService: AuthService,
    private feedbackService: FeedbackService,
    private ysToast: YsToastService,
    private fileService: UploadFilesService
  ) {}

  ngOnInit(): void {}

  sendFeedback(): void {
    this.errorsTrigger = true;
    if (this.feedbackForm.valid) {
      let formData = new FormData();
      let body: Feedback = {
        subject: this.commonService.customTrim(
          this.feedbackForm.controls.subject.value
        ),
        message: this.commonService.customTrim(
          this.feedbackForm.controls.message.value
        ),
      };

      formData.append('file', this.feedbackForm.controls.fileData.value);
      formData.append('json', new Blob([JSON.stringify(body)]));

      this.feedbackForm.disable();

      this.feedbackService.createFeedback(formData).subscribe(
        (data: any) => {
          this.feedbackForm.reset();
          this.feedbackForm.enable();
          this.errorsTrigger = false;

          this.ysToast.showMessage({
            title: 'Success!',
            text: 'Feedback request success created!',
            typeToast: 'success',
            duration: 800,
          });
        },
        (error) => {
          if (error.error.type == 'limitForTimeRequest') {
            this.remainingTimeError = error.error.value;
          }
          this.feedbackForm.enable();
        }
      );
    }
  }

  resetErrorsForm() {
    this.errorsTrigger = false;
    this.remainingTimeError = 0;
  }

  getFormatedTime(seconds: number) {
    let formatToTwo = (value: number) => {
      return value < 10 ? '0' + value : value;
    };

    let totalMinutes = seconds / 60;
    let hours = Math.round(totalMinutes / 60);
    let minutes = Math.round(totalMinutes % 60);

    return formatToTwo(hours) + 'h. ' + formatToTwo(minutes) + 'm.';
  }

  // ----- GET EXCEL DATES -------
  uploadFile(event: Event) {
    let fileData = this.fileService.fileUpload(event);
    console.log(fileData);
    this.feedbackForm.controls.fileData.setValue(fileData);
  }

  resetTimeLimit() {
    this.feedbackService.resetTimeLimit().subscribe((data: any) => {
      if (data && this.authService.user)
        this.authService.user.last_date_request = null;
    });
  }
}
