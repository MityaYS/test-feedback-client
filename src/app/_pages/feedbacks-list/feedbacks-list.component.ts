import { environment } from './../../../environments/environment.prod';
import { Feedback } from './../../_models/feedback.model';
import { FeedbackService } from './../../_services/feedback.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-feedbacks-list',
  templateUrl: './feedbacks-list.component.html',
  styleUrls: ['./feedbacks-list.component.scss'],
})
export class FeedbacksListComponent implements OnInit {
  feedbacks: Feedback[] = [];
  activeFeedback: Feedback | null = null;
  isLoad = false;
  isShowFeedbackInfo = false;

  totalPages = 1;
  currentPage = 1;
  constructor(private feedbackService: FeedbackService) {}

  ngOnInit(): void {
    this.getFeedbacksByPage();
  }

  getFeedbacksByPage(page: number = 1) {
    this.currentPage = page;
    this.isLoad = true;
    this.feedbackService.getFeedbacks(this.currentPage).subscribe(
      (data: any) => {
        console.log('DATA: ', data);
        this.feedbacks = data.feedbacks;
        this.totalPages = data.total_pages;
        this.isLoad = false;

        // this.selectFeedback(this.feedbacks[0]);
      },
      (error) => {
        this.isLoad = false;
      }
    );
  }

  selectFeedback(feedback: Feedback) {
    this.activeFeedback = JSON.parse(JSON.stringify(feedback));
    this.isShowFeedbackInfo = true;
  }

  toggleViewFeedback(feedback: Feedback, event: any = null) {
    if (event) event.stopPropagation();

    let body: any = {
      id: feedback.id,
      is_viewed: !feedback.is_viewed,
    };
    this.feedbackService.toggleViewFeedback(body).subscribe((data: any) => {
      if (this.activeFeedback) this.activeFeedback.is_viewed = data.is_viewed;

      let feedback = this.feedbacks.find((fb) => fb.id == data.id);
      if (feedback) feedback.is_viewed = data.is_viewed;
    });
  }

  getTotalPages() {
    return Array.from(Array(this.totalPages).keys());
  }

  getLinkForDownload(file_link: string) {
    return environment.url + file_link;
  }
}
