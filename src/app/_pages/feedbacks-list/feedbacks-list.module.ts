import { YsModalModule } from './../../_common/ys-modal/ys-modal.module';
import { FeedbacksListComponent } from './feedbacks-list.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FeedbacksListRoutingModule } from './feedbacks-list-routing.module';

@NgModule({
  declarations: [FeedbacksListComponent],
  imports: [CommonModule, FeedbacksListRoutingModule, YsModalModule],
})
export class FeedbacksListModule {}
