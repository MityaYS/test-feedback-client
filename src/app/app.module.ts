import { AppInterceptor } from './_services/app.interceptor';
import { CommonService } from './_services/common.service';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FeedbackPageComponent } from './_pages/feedback-page/feedback-page.component';
import { RegisterPageComponent } from './_pages/register-page/register-page.component';
import { AuthPageComponent } from './_pages/auth-page/auth-page.component';
import { MessagePageComponent } from './_pages/message-page/message-page.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FeedbacksListComponent } from './_pages/feedbacks-list/feedbacks-list.component';

@NgModule({
  declarations: [
    AppComponent,
    FeedbackPageComponent,
    RegisterPageComponent,
    AuthPageComponent,
    MessagePageComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  providers: [
    CommonService,
    { provide: HTTP_INTERCEPTORS, useClass: AppInterceptor, multi: true },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
