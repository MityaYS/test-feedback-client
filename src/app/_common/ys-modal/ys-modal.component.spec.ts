import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { YsModalComponent } from './ys-modal.component';

describe('YsModalComponent', () => {
  let component: YsModalComponent;
  let fixture: ComponentFixture<YsModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [YsModalComponent],
      imports: [IonicModule.forRoot()],
    }).compileComponents();

    fixture = TestBed.createComponent(YsModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
