import { FormsModule } from '@angular/forms';
import { YsModalComponent } from './ys-modal.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [
    YsModalComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
  ],
  exports: [YsModalComponent]
})
export class YsModalModule { }
