import { ValidationErrors, AbstractControl } from '@angular/forms';

export function comparePasswords(
  group: AbstractControl
): ValidationErrors | null {
  let password = group.get('password')?.value;
  let confirmPassword = group.get('repeatPassword')?.value;
  return password === confirmPassword ? null : { notIdentic: true };
}
